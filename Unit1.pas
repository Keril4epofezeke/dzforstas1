unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Button1: TButton;
 procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TField = class(TObject)
  private
    function GetAsString:string; virtual; abstract;
    procedure SetAsString(const Value:string);
              virtual; abstract;
  public
    property AsString:string read GetAsString
                         write SetAsString;
  end;

  TStringField=class(TField)
  private
    FData : string;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
end;
TIntegerField = class(TField)
  private
    FData : integer;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
end;

TFloatField=class(TField)
   private
    FData : extended;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
end;

TDateTimeField = class(TField)
  private
    FData : TDateTime;
    function GetAsString:string; override;
    procedure SetAsString(const Value:string);
              override;
end;

var
  Form1: TForm1;
  A: TstringField;

implementation


function TStringField.GetAsString:string;
begin
  Result := FData;
end;
procedure TStringField.SetAsString(const
                          Value:string);
begin
  FData := Value;
end;
function TIntegerField.GetAsString:string;
begin
  Result := IntToStr(FData);
end;
procedure TIntegerField.SetAsString(const
                          Value:string);
begin
  FData := StrToInt(Value);
end;

function TFloatField.GetAsString:string;
begin
  Result := FloatToStrF(FData,ffFixed,7,2);
end;
procedure TFloatField.SetAsString(const
                      Value:string);
begin
  FData := StrToFloat(Value);
end;
function TDateTimeField.GetAsString:string;
begin
  Result := DateTimeToStr(FData);
end;
procedure TDateTimeField.SetAsString(const
                        Value:string);
begin
  FData := StrToDateTime(Value);
end;

procedure ShowData(AField:TField);
begin
   Form1.Label1.Caption := AField.AsString;
end;


procedure TForm1.FormCreate(Sender: TObject);
begin
  A:=TstringField.Create;
end;


procedure TForm1.Button1Click(Sender: TObject);
begin
 A.AsString:=Edit1.Text;
 ShowData(A);
end;

end.

